let imgIndex = 1;
let image = document.querySelectorAll('img.image-to-show');

image[0].style.opacity = '1';
let goSlide = () =>{
    image.forEach(function (element, i) {
        element.style.opacity = '0';
        if (i === (imgIndex % image.length)){
            element.style.opacity = '1';
        } else {
            element.style.opacity = '0'
        }
    });
    imgIndex ++
};

const stopBtn = document.createElement('button');
const resumeBtn = document.createElement('button');

stopBtn.style.float = 'right';
stopBtn.innerText = 'Прекратить';
resumeBtn.style.float = 'right';
resumeBtn.innerText = 'Возобновить';

document.body.appendChild(stopBtn);
document.body.appendChild(resumeBtn);

let timerId = setInterval(goSlide, 1000);

stopBtn.onclick = () =>{
    clearInterval(timerId);
};

resumeBtn.onclick = ()=>{
    timerId = setInterval(goSlide, 1000);
};


































// let imagesList = document.querySelectorAll('.image-to-show');
// let stopButton = document.createElement('button');
// stopButton.innerHTML = 'Прекратить';
// document.body.appendChild(stopButton);
// let goButton = document.createElement('button');
// goButton.innerHTML = 'Продолжить';
// document.body.appendChild(goButton);
//
// imagesList.forEach((elem) => {
//     elem.style.display = 'none';
// });
// let index = 0;
// imagesList[index].style.display = 'block';
//
// function startSlider() {
//     imagesList[index].style.display = 'none';
//     index++;
//     if (index === imagesList.length) {
//         index = 0;
//     }
//     imagesList[index].style.display = 'block';
// }
//
// let timerId = setInterval(startSlider, 1000);
//
// stopButton.onclick = () => {
//     clearInterval(timerId);
// };
//
// goButton.onclick = () => {
//     clearInterval(timerId);
//     timerId = setInterval(startSlider, 1000);
//
// //console.log(timerId);
// };