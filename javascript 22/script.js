const createCircle = document.getElementById('createCircle');

createCircle.addEventListener('click', () =>{
    let size = prompt('Введите диаметр круга');
    const randomColor = ()=> {
        return `rgba(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.random()})`;
    };
    for(let i = 0; i<=100; i++){
        const circle = document.createElement('div');
        circle.className = 'circle'
        circle.style.display = 'inline-block';
        circle.style.backgroundColor = randomColor();
        circle.style.width = `${size}px`;
        circle.style.height = `${size}px`;
        circle.style.borderRadius = '50%';
        document.body.appendChild(circle);
    }
    document.body.addEventListener('click', (event) =>{
        if (event.target.classList.contains('circle')){
            event.target.remove()
        }else{
            return false
        }
    })
});


